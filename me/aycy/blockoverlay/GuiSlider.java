package me.aycy.blockoverlay;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.Minecraft;

import java.text.DecimalFormat;

import net.minecraft.client.gui.GuiButton;

public class GuiSlider extends GuiButton {
    private boolean isDragging;
    private float minValue;
    private float maxValue;
    private float sliderValue;
    private DecimalFormat decimalFormat;
    private String sliderPrefix;

    public GuiSlider(final int buttonId, final int x, final int y, final String sliderPrefix, final int minValue, final int maxValue, final int currentValue) {
        this(buttonId, x, y, sliderPrefix, minValue, maxValue, currentValue, "#");
    }

    public GuiSlider(final int buttonId, final int x, final int y, final String sliderPrefix, final float minValue, final float maxValue, final float currentValue) {
        this(buttonId, x, y, sliderPrefix, minValue, maxValue, currentValue, "#.00");
    }

    public GuiSlider(final int buttonId, final int x, final int y, final String sliderPrefix, final float minValue, final float maxValue, final float currentValue, final String pattern) {
        super(buttonId, x, y, sliderPrefix);
        this.isDragging = false;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.sliderValue = ((currentValue - minValue) / (maxValue - minValue));
        this.decimalFormat = new DecimalFormat(pattern);
        this.sliderPrefix = sliderPrefix;
        super.displayString = this.sliderPrefix + this.decimalFormat.format(this.sliderValue * (this.maxValue - this.minValue) + this.minValue);
    }

    protected int getHoverState(final boolean mouseOver) {
        return 0;
    }

    protected void mouseDragged(final Minecraft mc, final int mouseX, final int mouseY) {
        if (this.visible) {
            if (this.isDragging) {
                this.sliderValue = (float) (mouseX - (this.x + 4)) / (this.width - 8);
                this.updateSlider();
            }
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            this.drawTexturedModalRect(super.x + (int) (this.sliderValue * (super.width - 8)), super.y, 0, 66, 4, 20);
            this.drawTexturedModalRect(super.x + (int) (this.sliderValue * (super.width - 8)) + 4, super.y, 196, 66, 4, 20);
        }
    }

    public boolean mousePressed(final Minecraft mc, final int mouseX, final int mouseY) {
        if (super.mousePressed(mc, mouseX, mouseY)) {
            this.sliderValue = (mouseX - (this.x + 4)) / (this.width - 8);
            this.updateSlider();
            return this.isDragging = true;
        }
        return false;
    }

    public void mouseReleased(final int mouseX, final int mouseY) {
        this.isDragging = false;
    }

    public void updateSlider() {
        if (this.sliderValue < 0.0f) {
            this.sliderValue = 0.0f;
        } else if (this.sliderValue > 1.0f) {
            this.sliderValue = 1.0f;
        }
        super.displayString = this.sliderPrefix + this.decimalFormat.format(this.sliderValue * (this.maxValue - this.minValue) + this.minValue);
    }

    public float getSliderValue() {
        return (float) this.sliderValue;
    }
}
