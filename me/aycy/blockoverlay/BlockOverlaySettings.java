package me.aycy.blockoverlay;

import java.io.IOException;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class BlockOverlaySettings extends GuiScreen {
    private GuiButton buttonColor;
    private GuiButton buttonRender;
    private GuiButton buttonBack;
    private GuiSlider sliderWidth;

    public void initGui() {
        super.buttonList.add(this.sliderWidth = new GuiSlider(0, super.width / 2 - 100, super.height / 2 - 30, "Line width: ", 2.0f, 5.0f, BlockOverlay.lineWidth));
        super.buttonList.add(this.buttonColor = new GuiButton(1, super.width / 2 - 100, super.height / 2, "Color"));
        super.buttonList.add(this.buttonRender = new GuiButton(2, super.width / 2 - 100, super.height / 2 + 30, "Always render: " + (BlockOverlay.alwaysRender ? "On" : "Off")));
        super.buttonList.add(this.buttonBack = new GuiButton(3, super.width / 2 - 100, super.height / 2 + 80, "Back"));
    }

    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        super.drawDefaultBackground();
        this.sliderWidth.drawButton(super.mc, mouseX, mouseY, partialTicks);
        this.buttonColor.drawButton(super.mc, mouseX, mouseY, partialTicks);
        this.buttonRender.drawButton(super.mc, mouseX, mouseY, partialTicks);
        this.buttonBack.drawButton(super.mc, mouseX, mouseY, partialTicks);
    }

    public void actionPerformed(final GuiButton button) throws IOException {
        switch (button.id) {
            case 0: {
                BlockOverlay.lineWidth = this.sliderWidth.getSliderValue() * 3.0f + 2.0f;
                break;
            }
            case 1: {
                Minecraft.getMinecraft().displayGuiScreen((GuiScreen) new BlockOverlayColor());
                break;
            }
            case 2: {
                BlockOverlay.alwaysRender = !BlockOverlay.alwaysRender;
                this.buttonRender.displayString = "Always render: " + (BlockOverlay.alwaysRender ? "On" : "Off");
                break;
            }
            case 3: {
                Minecraft.getMinecraft().displayGuiScreen((GuiScreen) new BlockOverlayGui());
                break;
            }
        }
    }

    public void mouseClickMove(final int mouseX, final int mouseY, final int clickedMouseButton, final long timeSinceLastClick) {
        BlockOverlay.lineWidth = this.sliderWidth.getSliderValue() * 3.0f + 2.0f;
    }

    public void onGuiClosed() {
        BlockOverlay.saveConfig();
    }
}
