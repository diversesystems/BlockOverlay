package me.aycy.blockoverlay;

import net.minecraft.command.CommandException;

import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommandSender;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class BlockOverlayCommand extends CommandBase {
    public String getName() {
        return "blockoverlay";
    }

    public List<String> getAliases() {
        final List<String> aliases = new ArrayList<String>();
        aliases.add("boverlay");
        return aliases;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            BlockOverlay.openGui = true;
        } else {
            Minecraft.getMinecraft().player.sendMessage(new TextComponentString(TextFormatting.RED + this.getUsage(sender)));
        }
    }

    public String getUsage(final ICommandSender sender) {
        return "/blockoverlay";
    }

    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
}
