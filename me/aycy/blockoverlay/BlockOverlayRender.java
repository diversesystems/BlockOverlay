package me.aycy.blockoverlay;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.math.*;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderGlobal;
import java.awt.Color;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.Blocks;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.world.WorldSettings;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;

public class BlockOverlayRender
{
    @SubscribeEvent
    public void onDrawBlockHighlight(final DrawBlockHighlightEvent event) {
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().world == null || (!Minecraft.getMinecraft().playerController.getCurrentGameType().equals((Object)WorldSettings.getGameTypeById(0)) && !Minecraft.getMinecraft().playerController.getCurrentGameType().equals((Object)WorldSettings.getGameTypeById(1)))) {
            return;
        }
        if (BlockOverlay.mode.equals(BlockOverlayMode.DEFAULT)) {
            return;
        }
        event.setCanceled(true);
        if (BlockOverlay.mode.equals(BlockOverlayMode.NONE)) {
            return;
        }
        if (Minecraft.getMinecraft().objectMouseOver.typeOfHit.equals((Object)RayTraceResult.Type.BLOCK))
            this.drawOverlay();
        if(Minecraft.getMinecraft().objectMouseOver.typeOfHit.equals((Object)RayTraceResult.Type.ENTITY))
            this.drawEOverlay();
    }

    @SubscribeEvent
    public void onRenderWorldLast(final RenderWorldLastEvent event) {
        if (Minecraft.getMinecraft().player == null || Minecraft.getMinecraft().world == null || (!Minecraft.getMinecraft().playerController.getCurrentGameType().equals((Object)WorldSettings.getGameTypeById(2)) && !Minecraft.getMinecraft().playerController.getCurrentGameType().equals((Object)WorldSettings.getGameTypeById(3)))) {
            return;
        }
        if (BlockOverlay.mode.equals(BlockOverlayMode.NONE) || BlockOverlay.mode.equals(BlockOverlayMode.DEFAULT)) {
            return;
        }
        if (BlockOverlay.alwaysRender) {
            if (Minecraft.getMinecraft().objectMouseOver.typeOfHit.equals((Object)RayTraceResult.Type.BLOCK))
                this.drawOverlay();

            if(Minecraft.getMinecraft().objectMouseOver.typeOfHit.equals((Object)RayTraceResult.Type.ENTITY))
                this.drawEOverlay();
        }
    }

    public void drawEOverlay() {

        RayTraceResult position = Minecraft.getMinecraft().objectMouseOver;

        if (position == null || !position.typeOfHit.equals((Object)RayTraceResult.Type.ENTITY)) {
            return;
        }

        GlStateManager.pushMatrix();
        GlStateManager.depthMask(false);
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GL11.glLineWidth(BlockOverlay.lineWidth);

        final AxisAlignedBB box = position.entityHit.getEntityBoundingBox().offset(-Minecraft.getMinecraft().getRenderManager().viewerPosX, -Minecraft.getMinecraft().getRenderManager().viewerPosY, -Minecraft.getMinecraft().getRenderManager().viewerPosZ).grow(0.0010000000474974513, 0.0010000000474974513, 0.0010000000474974513);

        if (BlockOverlay.mode.equals(BlockOverlayMode.OUTLINE)) {
            if (BlockOverlay.isChroma) {
                final double millis = (double)(System.currentTimeMillis() % (10000L / BlockOverlay.chromaSpeed) / (10000.0f / BlockOverlay.chromaSpeed));
                final Color color = Color.getHSBColor((float)millis, 0.8f, 0.8f);
//                GL11.glColor4f((float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, BlockOverlay.alpha);
                RenderGlobal.drawSelectionBoundingBox(box,(float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, BlockOverlay.alpha);
            }
            else {
//                GL11.glColor4f(BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, BlockOverlay.alpha);
                RenderGlobal.drawSelectionBoundingBox(box,BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, BlockOverlay.alpha);
            }
        }
        else if (BlockOverlay.isChroma) {
            final double millis = (double)(System.currentTimeMillis() % (10000L / BlockOverlay.chromaSpeed) / (10000.0f / BlockOverlay.chromaSpeed));
            final Color color = Color.getHSBColor((float)millis, 0.8f, 0.8f);
            GL11.glColor4f((float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, 1.0f);
            RenderGlobal.drawSelectionBoundingBox(box,(float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, 1.0f);
            GL11.glColor4f((float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, BlockOverlay.alpha);
            this.drawFilledBoundingBox(box);
        }
        else {
            GL11.glColor4f(BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, 1.0f);
            RenderGlobal.drawSelectionBoundingBox(box,BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, 1.0f);
            GL11.glColor4f(BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, BlockOverlay.alpha);
            this.drawFilledBoundingBox(box);
        }
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
        GlStateManager.depthMask(true);
        GlStateManager.popMatrix();
    }

    public void drawOverlay() {

        final RayTraceResult position = Minecraft.getMinecraft().player.rayTrace(6.0, 0.0f);
        if (position == null || !position.typeOfHit.equals((Object)RayTraceResult.Type.BLOCK)) {
            return;
        }
        final Block block = Minecraft.getMinecraft().player.world.getBlockState(position.getBlockPos()).getBlock();
        if (block == null || block.equals(Blocks.AIR) || block.equals(Blocks.BARRIER) || block.equals(Blocks.WATER) || block.equals(Blocks.FLOWING_WATER) || block.equals(Blocks.LAVA) || block.equals(Blocks.FLOWING_LAVA)) {
            return;
        }
        GlStateManager.pushMatrix();
        GlStateManager.depthMask(false);
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
        GL11.glLineWidth(BlockOverlay.lineWidth);

        final AxisAlignedBB box = Minecraft.getMinecraft().player.world.getBlockState(position.getBlockPos()).getSelectedBoundingBox((World)Minecraft.getMinecraft().world, position.getBlockPos()).offset(-Minecraft.getMinecraft().getRenderManager().viewerPosX, -Minecraft.getMinecraft().getRenderManager().viewerPosY, -Minecraft.getMinecraft().getRenderManager().viewerPosZ).grow(0.0010000000474974513, 0.0010000000474974513, 0.0010000000474974513);
        if (BlockOverlay.mode.equals(BlockOverlayMode.OUTLINE)) {
            if (BlockOverlay.isChroma) {
                final double millis = (double)(System.currentTimeMillis() % (10000L / BlockOverlay.chromaSpeed) / (10000.0f / BlockOverlay.chromaSpeed));
                final Color color = Color.getHSBColor((float)millis, 0.8f, 0.8f);
//                GL11.glColor4f((float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, BlockOverlay.alpha);
                RenderGlobal.drawSelectionBoundingBox(box,(float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, BlockOverlay.alpha);
            }
            else {
//                GL11.glColor4f(BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, BlockOverlay.alpha);
                RenderGlobal.drawSelectionBoundingBox(box,BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, BlockOverlay.alpha);
            }
        }
        else if (BlockOverlay.isChroma) {
            final double millis = (double)(System.currentTimeMillis() % (10000L / BlockOverlay.chromaSpeed) / (10000.0f / BlockOverlay.chromaSpeed));
            final Color color = Color.getHSBColor((float)millis, 0.8f, 0.8f);
            GL11.glColor4f((float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, 1.0f);
            RenderGlobal.drawSelectionBoundingBox(box,(float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, 1.0f);
            GL11.glColor4f((float)color.getRed() / 255.0f, (float)color.getGreen() / 255.0f, (float)color.getBlue() / 255.0f, BlockOverlay.alpha);
            this.drawFilledBoundingBox(box);
        }
        else {
            GL11.glColor4f(BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, 1.0f);
            RenderGlobal.drawSelectionBoundingBox(box,BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, 1.0f);
            GL11.glColor4f(BlockOverlay.red, BlockOverlay.green, BlockOverlay.blue, BlockOverlay.alpha);
            this.drawFilledBoundingBox(box);
        }
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
        GlStateManager.depthMask(true);
        GlStateManager.popMatrix();
    }

    public void drawFilledBoundingBox(final AxisAlignedBB box) {
        final Tessellator tessellator = Tessellator.getInstance();
        final BufferBuilder worldRenderer = tessellator.getBuffer();
        worldRenderer.begin(7, DefaultVertexFormats.POSITION);
        worldRenderer.pos(box.minX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.maxZ).endVertex();
        tessellator.draw();
        worldRenderer.begin(7, DefaultVertexFormats.POSITION);
        worldRenderer.pos(box.maxX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.maxZ).endVertex();
        tessellator.draw();
        worldRenderer.begin(7, DefaultVertexFormats.POSITION);
        worldRenderer.pos(box.minX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.minZ).endVertex();
        tessellator.draw();
        worldRenderer.begin(7, DefaultVertexFormats.POSITION);
        worldRenderer.pos(box.minX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.minZ).endVertex();
        tessellator.draw();
        worldRenderer.begin(7, DefaultVertexFormats.POSITION);
        worldRenderer.pos(box.minX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.minZ).endVertex();
        tessellator.draw();
        worldRenderer.begin(7, DefaultVertexFormats.POSITION);
        worldRenderer.pos(box.minX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.maxZ).endVertex();
        worldRenderer.pos(box.minX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.minX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.minZ).endVertex();
        worldRenderer.pos(box.maxX, box.maxY, box.maxZ).endVertex();
        worldRenderer.pos(box.maxX, box.minY, box.maxZ).endVertex();
        tessellator.draw();
    }
}
