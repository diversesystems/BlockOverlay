package me.aycy.blockoverlay;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "blockoverlay", useMetadata = true)
public class BlockOverlay {
    protected static boolean alwaysRender;
    protected static boolean isChroma;
    protected static boolean openGui;
    protected static float lineWidth;
    protected static float red;
    protected static float green;
    protected static float blue;
    protected static float alpha;
    protected static int chromaSpeed;
    protected static BlockOverlayMode mode;

    @Mod.EventHandler
    public void onInit(final FMLInitializationEvent event) {
        BlockOverlay.alwaysRender = false;
        BlockOverlay.isChroma = false;
        BlockOverlay.openGui = false;
        BlockOverlay.lineWidth = 2.0f;
        BlockOverlay.red = 1.0f;
        BlockOverlay.green = 1.0f;
        BlockOverlay.blue = 1.0f;
        BlockOverlay.alpha = 1.0f;
        BlockOverlay.chromaSpeed = 1;
        BlockOverlay.mode = BlockOverlayMode.DEFAULT;
        MinecraftForge.EVENT_BUS.register((Object) this);
        MinecraftForge.EVENT_BUS.register((Object) new BlockOverlayRender());
        ClientCommandHandler.instance.registerCommand((ICommand) new BlockOverlayCommand());
        this.loadConfig();
    }

    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        if (event.phase.equals((Object) TickEvent.Phase.END) && BlockOverlay.openGui) {
            BlockOverlay.openGui = false;
            Minecraft.getMinecraft().displayGuiScreen((GuiScreen) new BlockOverlayGui());
        }
    }

    public void loadConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + "/config/blockOverlay.cfg");
            if (file.exists()) {
                final BufferedReader reader = new BufferedReader(new FileReader(file));
                final String name = reader.readLine();
                for (final BlockOverlayMode mode : BlockOverlayMode.values()) {
                    if (mode.name.equals(name)) {
                        BlockOverlay.mode = mode;
                        break;
                    }
                }
                BlockOverlay.red = Float.parseFloat(reader.readLine());
                BlockOverlay.green = Float.parseFloat(reader.readLine());
                BlockOverlay.blue = Float.parseFloat(reader.readLine());
                BlockOverlay.alpha = Float.parseFloat(reader.readLine());
                BlockOverlay.alwaysRender = reader.readLine().equals("true");
                BlockOverlay.isChroma = reader.readLine().equals("true");
                BlockOverlay.chromaSpeed = Integer.parseInt(reader.readLine());
                BlockOverlay.lineWidth = Float.parseFloat(reader.readLine());
                reader.close();
            }
        } catch (Exception exception) {
            System.out.println("Error occurred while loading BlockOverlay configuration");
            exception.printStackTrace();
        }
    }

    public static void saveConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + "/config/blockOverlay.cfg");
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(BlockOverlay.mode.name + "\r\n" + BlockOverlay.red + "\r\n" + BlockOverlay.green + "\r\n" + BlockOverlay.blue + "\r\n" + BlockOverlay.alpha + "\r\n" + BlockOverlay.alwaysRender + "\r\n" + BlockOverlay.isChroma + "\r\n" + BlockOverlay.chromaSpeed + "\r\n" + BlockOverlay.lineWidth);
            writer.close();
        } catch (Exception exception) {
            System.out.println("Error occurred while saving BlockOverlay configuration");
            exception.printStackTrace();
        }
    }
}
